﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Chapter
{
	string m_mainText;

	ChapterOption m_option_A;
	ChapterOption m_option_B;
	ChapterOption m_option_C;
	ChapterOption m_option_D;

	public Chapter (string mainText)
	{
		m_mainText = mainText;
	}

	public void AddButton_A (string name, Chapter nextChapter)
	{
		m_option_A = new ChapterOption(name,nextChapter);
	}
		
	public void AddButton_B (string name, Chapter nextChapter)
	{
		m_option_B = new ChapterOption(name,nextChapter);
	}

	public void AddButton_C (string name, Chapter nextChapter)
	{
		m_option_C = new ChapterOption(name,nextChapter);
	}


	public void AddButton_D (string name, Chapter nextChapter)
	{
		m_option_D = new ChapterOption(name,nextChapter);
	}

	public void UpdateWindow(Text textObject)
	{
		textObject.text = m_mainText;
	}

	public void UpdateWindowButton_A(Text buttonText)
	{
		if (m_option_A != null) 
		{
			m_option_A.UpdateButton (buttonText);
		} 
		else 
		{
			buttonText.text = "";
		}
	}
		
	public void UpdateWindowButton_B(Text buttonText)
	{
		if (m_option_B != null) 
		{
			m_option_B.UpdateButton (buttonText);
		} 
		else 
		{
			buttonText.text = "";
		}
	}

	public void UpdateWindowButton_C(Text buttonText)
	{
		if (m_option_C != null) 
		{
			m_option_C.UpdateButton (buttonText);
		} 
		else 
		{
			buttonText.text = "";
		}
	}

	public void UpdateWindowButton_D(Text buttonText)
	{
		if (m_option_D != null) 
		{
			m_option_D.UpdateButton (buttonText);
		} 
		else 
		{
			buttonText.text = "";
		}
	}

	public Chapter ButtonPressed_A()
	{
		if (m_option_A != null) 
		{
			return m_option_A.ButtonPressed ();
		} 
		else 
		{
			return null;
		}
	}

	public Chapter ButtonPressed_B()
	{
		if (m_option_B != null) {
			return m_option_B.ButtonPressed ();
		} else {
			return null;
		}
	}

	public Chapter ButtonPressed_C()
	{
		if (m_option_C != null) {
			return m_option_C.ButtonPressed ();
		} 
		else 
		{
			return null;
		}
	}

	public Chapter ButtonPressed_D()
	{
		if (m_option_D != null) {
			return m_option_D.ButtonPressed ();
		} 
		else 
		{
			return null;
		}
	}
}
